"""
snake_case.py
Coded by Harrison B. Dano <harrisondano@gmail.com>
"""
import re


def convert_to_snake_case(input_str):
    """
    Converts camel case string to snake case string
    """

    def replace(match):
        group = []
        group[:0] = match.group(0).lower()
        return '%s_%s' % (group[0], group[1])

    return re.sub(r'[a-z][A-Z]', replace, input_str).lower()


if __name__ == '__main__':
    assert convert_to_snake_case('ThisIsAwesome') == 'this_is_awesome'
    print("all passed")
