"""
is_properly_nested.py
Coded by Harrison B. Dano <harrisondano@gmail.com>
"""
import re


def is_properly_nested(input_str):
    """
    Tells if the string is properly nested with parenthesis,
    curly braces and brackets.
    """

    def check_layer(layer_str):
        """
        Check nesting in current layer
        """
        return (
            check_nesting(layer_str, '{', '}') and
            check_nesting(layer_str, '[', ']') and
            check_nesting(layer_str, '(', ')')
        )

    def check_nesting(content_str, opener, closer):
        """
        Check if braces, parenthesis or brackets,
        if there is, are properly nested
        """
        try:
            _pos = content_str.index(opener)
        except ValueError:
            # not found
            return True
        result = re.search('\%s(.*)\%s' % (opener, closer), content_str)
        if result is None:
            return False
        return check_layer(result.group()[1:-1].strip())

    return check_layer(input_str.strip())


if __name__ == "__main__":
    assert is_properly_nested('{[]()}') is True
    assert is_properly_nested('{[(])}') is False
    assert is_properly_nested('{[}') is False
    print("all passed")
