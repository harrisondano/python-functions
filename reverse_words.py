"""
reverse_words.py
Coded by Harrison B. Dano <harrisondano@gmail.com>
"""


def reverse_words(message):
    """
    Takes a message as a list of characters and reverses
    the order of the words in place
    """
    words = ''.join(message).split(' ')
    print('words before: %s' % words)
    words.reverse()
    print('words after: %s' % words)
    return string_to_list(' '.join(words))


def string_to_list(input_str):
    """
    Converts string to list of characters.
    """
    result = []
    result[:0] = input_str
    return result


if __name__ == '__main__':
    message = string_to_list('apple banana orange')
    print('message: %s' % message)
    result = ''.join(reverse_words(message))
    print('result: %s' % result)
    assert result == 'orange banana apple'
    print("all passed")
